package an.player.backend.data.daos;

import an.player.backend.data.models.CollectionVideo;
import an.player.backend.data.models.Video;
import an.player.backend.data.repositories.CollectionVideoRepo;
import an.player.backend.data.repositories.VideoRepo;
import an.player.backend.services.UuidGenerator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class VideoDao {
    private final VideoRepo videoRepo;
    private final CollectionVideoRepo collectionVideoRepo;

    public Video newVideo(String collectionId, Video video) {
        video.setId(UuidGenerator.next());
        video = videoRepo.save(video);
        collectionVideoRepo.save(new CollectionVideo(UuidGenerator.next(), collectionId, video.getId()));
        return video;
    }

    /*
    public void deleteVideo(String videoId) {
        todo problem wenn video aus collection geloescht wird, wird es auch aus player geloescht!!!
        Eigentlich sollten Videos ja eh unabhaengig von collection existieren.
        Vielleicht name und artist aus youtube link generieren und die description kann jeder für sich individuell festlegen
        Erstmal koennen videos somit nicht geloescht werden.
        //videoApi.deleteById(videoId);
    }
    */
}
