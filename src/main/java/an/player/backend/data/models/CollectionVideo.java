package an.player.backend.data.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class CollectionVideo {
    @Id
    private String id;

    @Column(name = "collection_id", nullable = false, unique = true)
    private String collectionId;

    @Column(name = "video_id", nullable = false, unique = true)
    private String videoId;

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CollectionVideo)) return false;
        if (id == null) return false;
        return id.equals(((CollectionVideo) obj).id);
    }
}
