package an.player.backend.web.wrappers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RegistrationRequest {
    private String email;
    private String userName;
    private String password;
}
