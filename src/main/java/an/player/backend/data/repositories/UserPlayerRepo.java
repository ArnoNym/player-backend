package an.player.backend.data.repositories;

import an.player.backend.data.models.UserPlayer;
import org.springframework.data.repository.CrudRepository;

public interface UserPlayerRepo extends CrudRepository<UserPlayer, String> {
}
