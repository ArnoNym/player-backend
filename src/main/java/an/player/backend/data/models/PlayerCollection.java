package an.player.backend.data.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class PlayerCollection {
    @Id
    private String id;

    @Column(name = "player_id", nullable = false, unique = true)
    private String playerId;

    @Column(name = "collection_id", nullable = false, unique = true)
    private String collectionId;

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PlayerCollection)) return false;
        if (id == null) return false;
        return id.equals(((PlayerCollection) obj).id);
    }
}
