package an.player.backend.web.wrappers;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class IdIdWrapper {
    private final String id1;
    private final String id2;
}
