package an.player.backend.data.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Collection {
    @Id
    private String id;

    @Column(nullable = false)
    private String name;

    private String description;

    public Collection(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @Transient
    private List<Video> videos = new ArrayList<>();

    @Transient
    private Set<Player> players = new HashSet<>();
}
