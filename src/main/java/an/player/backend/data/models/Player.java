package an.player.backend.data.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Player {
    @Id
    private String id;

    @Column(nullable = false)
    private String name;

    private String description = null;

    @Transient
    private Set<Collection> collections;

    @Transient
    private Set<Collection> activeCollections = new HashSet<>();

    @Transient
    private List<Video> videos = new ArrayList<>();

    public Player(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }
}
