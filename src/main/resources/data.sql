INSERT INTO user (id, name, password, email, created_time, last_active_time) VALUES
('1', 'aa', 'aa', null, '2007-04-30 13:10:02.047', null);

INSERT INTO Player (id, name, description) VALUES
('placeholder-id', 'placeholder-name', 'Replace this as soon as login is working!');

INSERT INTO user_player (id, user_id, player_id) VALUES
('2323', '1', 'placeholder-id');

/*
INSERT INTO Player (id, invite_id, version, last_used_time, created_time, name, password,
add_demander_bonus, max_opinion_value, note, offer_link_active, demander_link_active) VALUES
('11', 'i11', 1, '2000-01-01 00:00:00', '2000-01-01 00:00:00', 'BrettspielAG Essen', NULL, 10, 10, NULL, false, false),
('12', 'i12', 1, '2000-01-01 00:00:00', '2000-01-01 00:00:00', 'BrettspielAG Spielen', NULL, 10, 10, NULL, false, false);

INSERT INTO Decision (id, invite_id, version, last_used_time, created_time, name, password,
add_demander_bonus, max_opinion_value, note, offer_link_active, demander_link_active) VALUES
('11', 'i11', 1, '2000-01-01 00:00:00', '2000-01-01 00:00:00', 'BrettspielAG Essen', NULL, 10, 10, NULL, false, false),
('12', 'i12', 1, '2000-01-01 00:00:00', '2000-01-01 00:00:00', 'BrettspielAG Spielen', NULL, 10, 10, NULL, false, false);

INSERT INTO Demander (id, decision_id, name, has_To_Be_Included) VALUES
('11', '11', 'Tom', FALSE),
('12', '11', 'Tim', FALSE),
('13', '11', 'Rolph', FALSE),
('14', '11', 'Martin', FALSE),

INSERT INTO Offer (Id, Decision_Id, Name, Has_To_Be_Included, Available, Min_Group_Size, Max_Group_Size, Note) VALUES
(11, 11, 'Schach', FALSE, 1, 1, null, ''),
(12, 11, 'Risiko', FALSE, 2, 1, 1, ''),
(13, 11, 'Carcassone', FALSE, 1, 0, null, ''),

(14, 12, 'Kochen', FALSE, 1, 0, null, ''),
(15, 12, 'Ballet', FALSE, 1, 1, null, '');

INSERT INTO Opinion (Id, Demander_Id, Offer_Id, Value, Note) VALUES
(1, 11, 11, 5, ''),
(2, 11, 12, 9, ''),
(3, 11, 13, 7, ''),
(4, 12, 11, 9, ''),
(5, 12, 12, 9, ''),
(6, 12, 13, 4, ''),
(7, 13, 11, 5, ''),
(8, 13, 12, 9, ''),
(9, 13, 13, 7, ''),
(10, 14, 11, 5, ''),
(11, 14, 12, 9, ''),
(12, 14, 13, 7, ''),

(13, 15, 14, 5, ''),
(15, 15, 5, ''),
(16, 14, 5, ''),
(16, 15, 5, '');
*/