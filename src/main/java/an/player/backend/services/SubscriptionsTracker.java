package an.player.backend.services;

import an.player.backend.constants.ReactivityConstants;
import an.player.backend.web.wrappers.Wrapper;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
public class SubscriptionsTracker {
    private final Map<String, Set<String>> map = new HashMap<>();

    public Set<String> getSubscriberTokens(String id) {
        return map.get(id);
    }

    public Set<String> getDestinations(ReactivityConstants.Reactivity reactivity, String id) {
        assert map.size() >= 1;
        Set<String> subscriptionDestinations = new HashSet<>();
        for (String subscriberToken : getSubscriberTokens(id)) {
            subscriptionDestinations.add(reactivity.createDestination(subscriberToken));
        }
        return subscriptionDestinations;
    }

    public void put(String id, String subscriberToken) {
        Set<String> tokens = map.computeIfAbsent(id, k -> new HashSet<>());
        tokens.add(subscriberToken);
        map.put(id, tokens);
    }

    public void removeId(String id) {
        map.remove(id);
    }

    public void removeSubscriberToken(String subscriberToken) {
        for (Set<String> accessTokens : map.values()) {
            accessTokens.remove(subscriberToken);
        }
    }
}
