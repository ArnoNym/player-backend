package an.player.backend.data.repositories;

import an.player.backend.data.models.CollectionVideo;
import an.player.backend.data.models.PlayerCollection;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface CollectionVideoRepo extends CrudRepository<CollectionVideo, String> {
    @Transactional
    void deleteAllByCollectionIdAndVideoId(String collectionId, String videoId);

    @Transactional
    void deleteAllByCollectionId(String collectionId);
}
