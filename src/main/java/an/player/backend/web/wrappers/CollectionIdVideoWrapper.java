package an.player.backend.web.wrappers;

import an.player.backend.data.models.Video;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CollectionIdVideoWrapper {
    private final String collectionId;
    private final Video video;
}
