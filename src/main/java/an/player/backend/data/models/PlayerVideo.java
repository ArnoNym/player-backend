package an.player.backend.data.models;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class PlayerVideo {
    @Id
    private String id;

    @Column(name = "player_id", nullable = false, unique = true)
    private String playerId;

    @Column(name = "video_id", nullable = false, unique = true)
    private String videoId;

    @Column(nullable = false, unique = true)
    private Integer orderIndex;

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PlayerVideo)) return false;
        if (id == null) return false;
        return id.equals(((PlayerVideo) obj).id);
    }
}