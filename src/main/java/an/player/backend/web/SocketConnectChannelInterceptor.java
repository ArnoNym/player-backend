package an.player.backend.web;

import an.player.backend.services.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.core.Authentication;

/**
 * https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/web.html#websocket-stomp-authentication-token-based
 */

//@Order(Ordered.HIGHEST_PRECEDENCE + 99)
public class SocketConnectChannelInterceptor implements ChannelInterceptor {
    @Autowired
    private JwtUtil jwtUtil;

    @Override
    public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
        System.out.println("postSend");
    }

    @Override
    public void afterSendCompletion(Message<?> message, MessageChannel channel, boolean sent, Exception ex) {
        System.out.println("afterSendCompletion");
    }

    @Override
    public boolean preReceive(MessageChannel channel) {
        System.out.println("preReceive");
        return false;
    }

    @Override
    public Message<?> postReceive(Message<?> message, MessageChannel channel) {
        System.out.println("postReceive");
        return null;
    }

    @Override
    public void afterReceiveCompletion(Message<?> message, MessageChannel channel, Exception ex) {
        System.out.println("afterReceiveCompletion");

    }

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) { /* TODO Das macht momentan gar nichts.
    Sollte fuer den Workaround in WebSecurityConfiguration eingesetzt werden. Das Problem ist, dass es erst nach
    Websucrity aufgerufen wird, also die Anfragen schon gefiltert wurden also diese Methode nie aufgerufen wird */
        
        System.out.println("preSend ::: configureClientInboundChannel");
        StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
        if (StompCommand.CONNECT.equals(accessor.getCommand())) {
            String token = accessor.getFirstNativeHeader("x-auth-token");
            Authentication user = jwtUtil.getAuthenticationFromToken(token); // https://stackoverflow.com/questions/43475884/spring-security-token-based-authentication-for-sockjs-stomp-web-socket
            System.out.println("configureClientInboundChannel ::: user.getName()="+user.getName());
            accessor.setUser(user);
        }
        return message;
    }
}
