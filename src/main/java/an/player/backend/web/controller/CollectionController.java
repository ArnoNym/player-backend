package an.player.backend.web.controller;

import an.player.backend.data.daos.CollectionDao;
import an.player.backend.data.daos.VideoDao;
import an.player.backend.data.models.Collection;
import an.player.backend.data.models.Video;
import an.player.backend.web.wrappers.CollectionIdVideoWrapper;
import an.player.backend.web.wrappers.IdIdWrapper;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class CollectionController {
    private final CollectionDao collectionDao;
    private final VideoDao videoDao;

    @PostMapping("/deleteVideo")
    public void deleteVideo(@RequestBody IdIdWrapper collectionIdVideoIdWrapper) {
        collectionDao.removeVideo(collectionIdVideoIdWrapper.getId1(), collectionIdVideoIdWrapper.getId2());
    }

    @PostMapping("/addVideo")
    public Video addVideo(@RequestBody CollectionIdVideoWrapper collectionIdVideoWrapper) {
        return videoDao.newVideo(collectionIdVideoWrapper.getCollectionId(), collectionIdVideoWrapper.getVideo());
    }
}
