package an.player.backend.data.repositories;

import an.player.backend.data.models.Collection;
import an.player.backend.data.models.Video;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface VideoRepo extends CrudRepository<Video, String> {
    Video findByLink(String link);

    @Query(
            value = "SELECT new Video(v.id, v.link, v.title, v.artist, v.description) " +
                    "FROM Video v, CollectionVideo cv " +
                    "WHERE v.id = cv.videoId AND cv.collectionId = ?1"
    )
    List<Video> findAllByCollectionId(String collectionId);

    @Query(
            value = "SELECT new Video(v.id, v.link, v.title, v.artist, v.description) " +
                    "FROM Video v, PlayerVideo pv " +
                    "WHERE v.id = pv.videoId AND pv.playerId = ?1"
                    //TODO + "ORDER BY pv.orderIndex"
    )
    List<Video> findAllByPlayerId(String playerId);
}
