package an.player.backend.web.filters;

import an.player.backend.data.daos.UserDao;
import an.player.backend.services.JwtUtil;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor

@Component
public class JwtFilter extends OncePerRequestFilter {
    private final JwtUtil jwtUtil;
    private final UserDao userDao;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {
        System.out.println("CALLED ::: JwtFilter.doFilterInternal");
        //System.out.println("Content-type="+httpServletRequest.newPushBuilder().setHeader("Content-type", "application/json; charset=UTF-8"));

        String authorizationHeader = httpServletRequest.getHeader("Authorization");

        System.out.println("authorizationHeader="+authorizationHeader);
        //String token = accessor.getFirstNativeHeader("x-auth-token");

        String token = null;
        String userName = null;

        if (authorizationHeader != null) {
            System.out.println("CALLED ::: authorizationHeader != null");
            token = authorizationHeader;
            userName = jwtUtil.extractUsername(token);
        }

        if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            System.out.println("CALLED ::: userName != null && SecurityContextHolder.getContext().getAuthentication() == null");
            UserDetails userDetails = userDao.loadUserByUsername(userName);

            //if (jwtUtil.validateToken(token, userDetails)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            //}
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

        /*
        hier koennte man die userId dem body hinzufuegen und dann beim fetchen der Daten pruefen ob diese
                auch vond em user verwendet werden duerfen
                NICHT NOETIG WENN MANN ZUM LOGIN SESSION VERWENDET: VON DENEN KANN MAN IM CONTROLLER GANZE EINFACH DIE SESSIONS ERHALTEN



                user loggen sich ein. Eine neue session entsteht.
        Es wird eine Map von userId auf SessionId erstellt
                Die Clienten erhalten die Session id und subscriben bei einem Socket, wobei die Session Id
                der Nutzername ist (https://stackoverflow.com/questions/37853727/where-user-comes-from-in-convertandsendtouser-works-in-sockjsspring-websocket?noredirect=1&lq=1))

        logout : loeschte das mapping

            daten fetchen: ganz normal (filter werden überwunden durch den login)

        Daten aendern sich im Server und müssen dem Clienten geschickt werden:
            Aus den Sessions die eingeloggten User erhalten
            SQL Anfrage welche User eingeloggt sind und von der Aenderung betroffen sind (evtl Reihenfolge mit 1. veratscuhen)
                Sende an die Websockts jeweils mit der SessionId als username
        */
}
