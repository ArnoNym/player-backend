package an.player.backend.data.repositories;

import an.player.backend.data.models.PlayerCollection;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Set;

public interface PlayerCollectionRepo extends CrudRepository<PlayerCollection, String> {
    @Transactional
    void deleteAllByCollectionId(String collectionId);

    Set<PlayerCollection> findAllByPlayerId(String playerId);

    @Transactional
    void deleteAllByPlayerIdAndCollectionId(String playerId, String collectionId);
}
