package an.player.backend.constants;

public class Constants {
    public static final String SOCKET_END_POINT = "/socket";

    public static final String ACCESS_TOKEN_IN_HEADER_NAME = "Authorization";

    public static final String LOGIN_TOKEN_SECRET = "7entenAufWiese"; // Todo in externer Datei oder so lagern
    public static final Integer LOGIN_TOKEN_EXPIRE_SECONDS = 60 * 60 * 10;
}
