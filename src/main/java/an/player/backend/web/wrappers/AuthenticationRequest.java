package an.player.backend.web.wrappers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
public class AuthenticationRequest {
    private String userName;
    private String password;
}
