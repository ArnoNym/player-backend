package an.player.backend.web.wrappers;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class AuthenticationResponse {
    private final String playerId;
    private final String playerName;
    private final String accessToken;
    private final String subscribeToken;
}
