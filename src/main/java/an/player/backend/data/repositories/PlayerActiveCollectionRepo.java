package an.player.backend.data.repositories;

import an.player.backend.data.models.PlayerActiveCollection;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface PlayerActiveCollectionRepo extends CrudRepository<PlayerActiveCollection, String> {
    @Transactional
    void deleteAllByCollectionId(String collectionId);

    @Transactional
    void deleteAllByPlayerIdAndCollectionId(String playerId, String collectionId);
}
