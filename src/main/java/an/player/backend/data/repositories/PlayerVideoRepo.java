package an.player.backend.data.repositories;

import an.player.backend.data.models.PlayerVideo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Optional;

public interface PlayerVideoRepo extends CrudRepository<PlayerVideo, String> {
    @Transactional
    void deleteAllByPlayerIdAndVideoId(String playerId, String videoId);

    @Query(
            value = "SELECT MAX(pv.orderIndex) " +
                    "FROM PlayerVideo pv " +
                    "WHERE pv.playerId = ?1"
    )
    Optional<Integer> findMaxOrderIndexByPlayerId(String playerId);
}
