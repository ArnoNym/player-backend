package an.player.backend.web.controller;

import an.player.backend.constants.Constants;
import an.player.backend.constants.ReactivityConstants;
import an.player.backend.data.daos.CollectionDao;
import an.player.backend.data.daos.PlayerDao;
import an.player.backend.data.models.Collection;
import an.player.backend.data.models.Video;
import an.player.backend.web.wrappers.IdIdWrapper;
import an.player.backend.web.wrappers.PlayerIdCollectionIdWrapper;
import an.player.backend.web.wrappers.PlayerIdCollectionWrapper;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
public class PlayerController {
    private final PlayerDao playerDao;
    private final CollectionDao collectionDao;
    private final ReactivityController reactivityController;

    @PostMapping("/newCollectionToPlayer")
    public Collection newCollectionToPlayer(@RequestBody PlayerIdCollectionWrapper wrapper) {
        wrapper.setCollection(collectionDao.newCollection(wrapper.getCollection()));
        playerDao.addCollection(wrapper.getPlayerId(), wrapper.getCollection().getId());
        reactivityController.push(ReactivityConstants.Reactivity.ADD_COLLECTION_TO_PLAYER, wrapper);
        return wrapper.getCollection(); //Nur fuer das einfuegen der handmade data. TODO loeschen
    }
    @PostMapping("/addCollectionToPlayer")
    public void addCollectionToPlayer(@RequestBody PlayerIdCollectionIdWrapper wrapper) {
        playerDao.addCollection(wrapper.getPlayerId(), wrapper.getCollectionId());
        reactivityController.push(ReactivityConstants.Reactivity.ADD_COLLECTION_TO_PLAYER, wrapper);
    }
    @PostMapping("/getCollectionsOfPlayer")
    public List<Collection> getCollectionsOfPlayer(@RequestBody String playerId,
                                                   @RequestHeader("Origin") String clientAddress,
                                                   @RequestHeader(Constants.ACCESS_TOKEN_IN_HEADER_NAME) String accessToken) {
        List<Collection> collections = playerDao.getCollections(playerId);
        reactivityController.addReactivity(collections.stream().map(Collection::getId).collect(Collectors.toSet()), clientAddress, accessToken);
        return collections;
    }
    @PostMapping("/removeCollectionFromPlayer")
    public void removeCollectionFromPlayer(@RequestBody PlayerIdCollectionIdWrapper wrapper) {
        playerDao.removeCollection(wrapper.getPlayerId(), wrapper.getCollectionId());
        reactivityController.push(ReactivityConstants.Reactivity.REMOVE_COLLECTION_FROM_PLAYER, wrapper);
        reactivityController.removeReactivity(wrapper.getCollectionId());
    }

    @PostMapping("/addActiveCollectionToPlayer")
    public void addActiveCollectionToPlayer(@RequestBody IdIdWrapper playerIdCollectionIdWrapper) {
        playerDao.addActiveCollection(playerIdCollectionIdWrapper.getId1(), playerIdCollectionIdWrapper.getId2());
    }
    /*
    Als Variable bei get collections mitschicken und dann ueber das Socket
    @PostMapping("/getActiveCollectionsOfPlayer")
    public Set<Collection> getActiveCollectionsOfPlayer(@RequestBody String playerId) {
        return playerDao.getActiveCollections(playerId);
    }
    */
    @PostMapping("/removeActiveCollectionFromPlayer")
    public void removeActiveCollectionFromPlayer(@RequestBody IdIdWrapper playerIdCollectionIdWrapper) {
        playerDao.addActiveCollection(playerIdCollectionIdWrapper.getId1(), playerIdCollectionIdWrapper.getId2());
    }

    @PostMapping("/newVideoToPlayer")
    public void newVideo(@RequestBody IdIdWrapper playerIdCollectionIdWrapper) {
        playerDao.addVideo(playerIdCollectionIdWrapper.getId1(), playerIdCollectionIdWrapper.getId2());
    }
    @PostMapping("/addVideoToPlayer")
    public void addVideoToPlayer(@RequestBody IdIdWrapper playerIdCollectionIdWrapper) {
        playerDao.addVideo(playerIdCollectionIdWrapper.getId1(), playerIdCollectionIdWrapper.getId2());
    }
    @PostMapping("/getVideosOfPlayer")
    public List<Video> getVideosOfPlayer(@RequestBody String playerId) {
        return playerDao.getVideos(playerId);
    }
    @PostMapping("/removeVideoFromPlayer")
    public void removeVideoFromPlayer(@RequestBody IdIdWrapper playerIdCollectionIdWrapper) {
        assert playerIdCollectionIdWrapper.getId1() != null;
        assert playerIdCollectionIdWrapper.getId2() != null;
        playerDao.removeVideo(playerIdCollectionIdWrapper.getId1(), playerIdCollectionIdWrapper.getId2());
    }

    /*

    @PostMapping("/newCollection")
    public void newCollection(@RequestBody PlayerIdCollectionWrapper wrapper,
                              @RequestHeader("Origin") String clientAddress,
                              @RequestHeader(Constants.ACCESS_TOKEN_IN_HEADER_NAME) String accessToken) {
        wrapper.setCollection(collectionDao.newCollection(collection));
        reactivityController.addReactivityAndPush(wrapper, clientAddress, accessToken);
    }
     */
}
