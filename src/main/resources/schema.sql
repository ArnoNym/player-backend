/*
CREATE DATABASE player;
USE player;
*/

CREATE TABLE user (
    id VARCHAR(255) NOT NULL,
	email VARCHAR(50),
	name VARCHAR(50) NOT NULL UNIQUE,
	password VARCHAR(50) NOT NULL,
	created_time TIMESTAMP,
	last_active_time TIMESTAMP,

    PRIMARY KEY(id)
);

CREATE TABLE user_player (
    id VARCHAR(255) NOT NULL,
	user_id VARCHAR(50) NOT NULL,
	player_id VARCHAR(50) NOT NULL UNIQUE,

    PRIMARY KEY(id)
);

CREATE TABLE video (
    id VARCHAR(255) NOT NULL,
	link VARCHAR(50) NOT NULL UNIQUE,
	title VARCHAR(200) NOT NULL,
	artist VARCHAR(50),
	description VARCHAR(100),

    PRIMARY KEY(id)
);

CREATE TABLE collection (
    id VARCHAR(255) NOT NULL,
	name VARCHAR(50) NOT NULL,
	description VARCHAR(50),

    PRIMARY KEY(id)
);

CREATE TABLE collection_video (
    id VARCHAR(255) NOT NULL,
    collection_id VARCHAR(255) NOT NULL,
    video_id VARCHAR(255) NOT NULL,

    PRIMARY KEY(id)
);

CREATE TABLE player (
    id VARCHAR(255) NOT NULL,
	name VARCHAR(50),
	description VARCHAR(100),

    PRIMARY KEY(id)
);

CREATE TABLE player_collection (
    id VARCHAR(255) NOT NULL,
    collection_id VARCHAR(255) NOT NULL,
    player_id VARCHAR(255) NOT NULL,

    PRIMARY KEY(id)
);

CREATE TABLE player_active_collection (
    id VARCHAR(255) NOT NULL,
    collection_id VARCHAR(255) NOT NULL,
    player_id VARCHAR(255) NOT NULL,

    PRIMARY KEY(id)
);

CREATE TABLE player_video (
    id VARCHAR(255) NOT NULL,
    player_id VARCHAR(255) NOT NULL,
    video_id VARCHAR(255) NOT NULL,
    order_index INTEGER NOT NULL,

    PRIMARY KEY(id)
);