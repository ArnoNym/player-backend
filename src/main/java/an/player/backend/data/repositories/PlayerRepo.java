package an.player.backend.data.repositories;

import an.player.backend.data.models.Collection;
import an.player.backend.data.models.Player;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlayerRepo extends CrudRepository<Player, String> {
    @Query(
            value = "SELECT new Player(p.id, p.name, p.description) " +
                    "FROM User u, UserPlayer up, Player p " +
                    "WHERE u.name = ?1 AND u.id = up.userId AND up.playerId = p.id"
    )
    List<Player> findAllByUserName(String userName);
}
