package an.player.backend.web.wrappers;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface Wrapper {
    @JsonIgnore
    String getSubscriptionId();
}
