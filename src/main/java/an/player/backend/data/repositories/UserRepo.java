package an.player.backend.data.repositories;

import an.player.backend.data.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepo extends CrudRepository<User, String> {
    User findFirstByName(String name);
}
