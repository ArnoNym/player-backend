package an.player.backend.web.controller;

import an.player.backend.data.daos.PlayerDao;
import an.player.backend.data.daos.UserDao;
import an.player.backend.data.models.Player;
import an.player.backend.data.models.User;
import an.player.backend.services.JwtUtil;
import an.player.backend.services.SubscribeTokenGenerator;
import an.player.backend.web.wrappers.AuthenticationRequest;
import an.player.backend.web.wrappers.AuthenticationResponse;
import an.player.backend.web.wrappers.RegistrationRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor

@RestController
public class AuthenticateController {
    private final JwtUtil jwtUtil;
    private final SubscribeTokenGenerator subscribeTokenGenerator;
    private final AuthenticationManager authenticationManager;
    private final PlayerDao playerDao;
    private final ReactivityController reactivityController;
    private final UserDao userDao;

    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> register(@RequestBody RegistrationRequest registrationRequest,
                                                           HttpServletRequest request) throws Exception {
        System.out.println("CALLED ::: register");

        User user = userDao.newUser(registrationRequest.getEmail(), registrationRequest.getUserName(), registrationRequest.getPassword());
        if (user == null) {
            throw new IllegalStateException("Could not create user!");
        }

        return generateToken(new AuthenticationRequest(registrationRequest.getUserName(), registrationRequest.getPassword()), request);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> generateToken(@RequestBody AuthenticationRequest authenticationRequest,
                                                                HttpServletRequest request) throws Exception {
        System.out.println("CALLED ::: authenticate");
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(), authenticationRequest.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new Exception("Invalid username or password.");
        }

        String accessToken = jwtUtil.generateAccessToken(authenticationRequest.getUserName());

        String subscribeToken = subscribeTokenGenerator.next(request.getRemoteAddr(), accessToken);

        Player player = playerDao.getPlayer(authenticationRequest.getUserName());

        reactivityController.addReactivity(player.getId(), subscribeToken);

        return ResponseEntity.ok(new AuthenticationResponse(player.getId(), player.getName(), accessToken, subscribeToken));
    }
}
