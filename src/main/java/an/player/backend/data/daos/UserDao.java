package an.player.backend.data.daos;

import an.player.backend.data.models.Player;
import an.player.backend.data.models.User;
import an.player.backend.data.models.UserPlayer;
import an.player.backend.data.repositories.PlayerRepo;
import an.player.backend.data.repositories.UserPlayerRepo;
import an.player.backend.data.repositories.UserRepo;
import an.player.backend.services.UuidGenerator;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;

@AllArgsConstructor

@Component
public class UserDao implements UserDetailsService {
    private final UserRepo userRepo;
    private final UserPlayerRepo userPlayerRepo;
    private final PlayerRepo playerRepo;

    public User newUser(String email, String userName, String password) throws UsernameNotFoundException {
        User user = userRepo.save(new User(UuidGenerator.next(), email, userName, password, Timestamp.from(Instant.now()), null));
        Player player = playerRepo.save(new Player(UuidGenerator.next(), userName+"Player", null));
        user.getPlayers().add(player);
        userPlayerRepo.save(new UserPlayer(UuidGenerator.next(), user.getId(), player.getId()));
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userRepo.findFirstByName(userName);
        System.out.println("USER WURDE GEFETCHED ::: user.getId="+user.getId()+", user.getName="+user.getName()+", user.getPassword="+user.getPassword());
        return new org.springframework.security.core.userdetails.User(
                user.getName(), user.getPassword(), new ArrayList<>()
        );
    }
}
