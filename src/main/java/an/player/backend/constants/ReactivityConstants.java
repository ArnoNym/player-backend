package an.player.backend.constants;

public class ReactivityConstants {
    public static final String PUBLIC_PREFIX = "/topic";
    public static final String PRIVATE_PREFIX = "/queue";

    public enum Reactivity {
        ADD_VIDEO_TO_COLLECTION(PRIVATE_PREFIX +"/add/video/to/collection"),
        ADD_VIDEO_TO_PLAYER(PRIVATE_PREFIX + "/add/video/to/player"),
        ADD_COLLECTION_TO_PLAYER(PRIVATE_PREFIX + "/add/collection/to/player"),

        REMOVE_VIDEO_FROM_COLLECTION(PRIVATE_PREFIX + "/remove/video/from/collection"),
        REMOVE_VIDEO_FROM_PLAYER(PRIVATE_PREFIX + "/remove/video/from/player"),
        REMOVE_COLLECTION_FROM_PLAYER(PRIVATE_PREFIX + "/remove/collection/from/player");

        public final String path;

        Reactivity(String path) {
            this.path = path;
        }

        public String createDestination(String accessToken) {
            return path + "/" + accessToken;
        }
    }
}
