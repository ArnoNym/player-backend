package an.player.backend.web.wrappers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@AllArgsConstructor
@Data
public class PlayerIdCollectionIdWrapper implements Wrapper {
    private String playerId;
    private String collectionId;

    @Override
    public String getSubscriptionId() {
        return playerId;
    }
}
