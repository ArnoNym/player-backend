package an.player.backend.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

@AllArgsConstructor

@Service
public class SubscribeTokenGenerator {
    private final JwtUtil jwtUtil;

    public String nextByUserName(String clientAddress, String userName) {
        return next(clientAddress, jwtUtil.generateAccessToken(userName));
    }

    public String next(String clientAddress, String accessToken) {
        return "SubscribeToken"+Objects.hash(clientAddress)+Objects.hash(accessToken); // Todo Algo um es absolut unique zu machen
    }
}
