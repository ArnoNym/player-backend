package an.player.backend.web.wrappers;

import an.player.backend.constants.ReactivityConstants;
import an.player.backend.data.models.Collection;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PlayerIdCollectionWrapper implements Wrapper {
    private String playerId;
    private Collection collection;

    @Override
    public String getSubscriptionId() {
        return playerId;
    }
}
