package an.player.backend.web.controller;

import an.player.backend.constants.ReactivityConstants;
import an.player.backend.services.SubscribeTokenGenerator;
import an.player.backend.services.SubscriptionsTracker;
import an.player.backend.web.wrappers.Wrapper;
import lombok.AllArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@AllArgsConstructor

@RestController
public class ReactivityController {
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final SubscriptionsTracker subscriptionsTracker;
    private final SubscribeTokenGenerator subscribeTokenGenerator;

    public void push(ReactivityConstants.Reactivity reactivity, Wrapper wrapper) {
        for (String subscriberDestination : subscriptionsTracker.getDestinations(reactivity, wrapper.getSubscriptionId())) {
            simpMessagingTemplate.convertAndSend(subscriberDestination, wrapper);
        }
    }

    public void removeReactivity(String id) {
        subscriptionsTracker.removeId(id);
    }

    public void addReactivity(String subscriptionId, String clientAddress, String accessToken) {
        addReactivity(subscriptionId, subscribeTokenGenerator.next(clientAddress, accessToken));
    }
    public void addReactivity(Collection<String> subscriptionIds, String clientAddress, String accessToken) {
        addReactivity(subscriptionIds, subscribeTokenGenerator.next(clientAddress, accessToken));
    }
    public void addReactivity(String subscriptionId, String subscribeToken) {
        subscriptionsTracker.put(subscriptionId, subscribeToken);
    }
    public void addReactivity(Collection<String> subscriptionIds, String subscribeToken) {
        for (String subscriptionId : subscriptionIds) {
            subscriptionsTracker.put(subscriptionId, subscribeToken);
        }
    }







    /*
    private SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/chat/{to}")
    public void sendMessage(@DestinationVariable String to, MessageModel message) {
        System.out.println("handling send message: " + message + " to: " + to);
        boolean isExists = UserStorage.getInstance().getUsers().contains(to);
        if (isExists) {
            simpMessagingTemplate.convertAndSend("/topic/messages/" + to, message);
        }
    }

    @GetMapping("/registration/{userName}")
    public ResponseEntity<Void> register(@PathVariable String userName) {
        System.out.println("handling register user request: " + userName);
        try {
            UserStorage.getInstance().setUser(userName);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/fetchAllUsers")
    public Set<String> fetchAll() {
        return UserStorage.getInstance().getUsers();
    }
    */
}
