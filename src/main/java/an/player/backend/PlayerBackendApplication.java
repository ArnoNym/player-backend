package an.player.backend;

import an.player.backend.data.models.User;
import an.player.backend.data.repositories.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor

@SpringBootApplication
public class PlayerBackendApplication {
    /*
    private final UserRepo userRepo;

    @PostConstruct
    public void initUsers() {
        List<User> users = Stream.of(
                new User("1", "an", "{noop}123", null, new Timestamp(System.currentTimeMillis()), null) // The {noop} is the password storage format
        ).collect(Collectors.toList());
        userRepo.saveAll(users);
    }
    */

    public static void main(String[] args) {
        SpringApplication.run(PlayerBackendApplication.class, args);
    }

}
