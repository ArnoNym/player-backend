package an.player.backend.data.daos;

import an.player.backend.data.models.Collection;
import an.player.backend.data.models.PlayerCollection;
import an.player.backend.data.models.Video;
import an.player.backend.data.repositories.*;
import an.player.backend.services.UuidGenerator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class CollectionDao {
    private final CollectionRepo collectionRepo;
    private final CollectionVideoRepo collectionVideoRepo;
    private final PlayerCollectionRepo playerCollectionRepo;
    private final PlayerActiveCollectionRepo playerActiveCollectionRepo;
    private final VideoRepo videoRepo;

    public void removeVideo(String collectionId, String videoId) {
        collectionVideoRepo.deleteAllByCollectionIdAndVideoId(collectionId, videoId);
    }

    public void deleteCollection(String collectionId) {
        collectionVideoRepo.deleteAllByCollectionId(collectionId);
        playerCollectionRepo.deleteAllByCollectionId(collectionId);
        playerActiveCollectionRepo.deleteAllByCollectionId(collectionId);
        collectionRepo.deleteById(collectionId);
    }

    public Collection newCollection(Collection collection) {
        collection.setId(UuidGenerator.next());
        collection = collectionRepo.save(collection);
        return collection;
    }

    public List<Video> getVideos(String collectionId) {
        return videoRepo.findAllByCollectionId(collectionId);
    }
}
