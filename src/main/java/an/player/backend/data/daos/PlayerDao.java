package an.player.backend.data.daos;

import an.player.backend.data.models.*;
import an.player.backend.data.repositories.*;
import an.player.backend.services.UuidGenerator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
@AllArgsConstructor
public class PlayerDao {
    private final PlayerActiveCollectionRepo playerActiveCollectionRepo;
    private final PlayerVideoRepo playerVideoRepo;
    private final VideoRepo videoRepo;
    private final CollectionRepo collectionRepo;
    private final PlayerCollectionRepo playerCollectionRepo;
    private final PlayerRepo playerRepo;

    public Player getPlayer(String userName) {
        List<Player> players = playerRepo.findAllByUserName(userName);
        if (players.size() != 1) throw new IllegalStateException();
        return players.get(0);
    }

    public void addCollection(String playerId, String collectionId) {
        playerCollectionRepo.save(new PlayerCollection(UuidGenerator.next(), playerId, collectionId));
    }
    public List<Collection> getCollections(String playerId) {
        List<Collection> collections = collectionRepo.findAllByPlayerId(playerId);
        fillCollectionsWithVideos(collections);
        return collections;
    }
    public void removeCollection(String playerId, String collectionId) {
        playerCollectionRepo.deleteAllByPlayerIdAndCollectionId(playerId, collectionId);
    }

    public void addActiveCollection(String playerId, String collectionId) {
        playerActiveCollectionRepo.save(new PlayerActiveCollection(UuidGenerator.next(), playerId, collectionId));
    }
    public Set<Collection> getActiveCollections(String playerId) {
        Set<Collection> collections = collectionRepo.findAllActiveByPlayerId(playerId);
        fillCollectionsWithVideos(collections);
        return collections;
    }
    public void removeActiveCollection(String playerId, String collectionId) {
        playerActiveCollectionRepo.deleteAllByPlayerIdAndCollectionId(playerId, collectionId);
    }

    public void addVideo(String playerId, String videoId) {
        Integer maxOrderIndex = playerVideoRepo.findMaxOrderIndexByPlayerId(playerId).orElse(0);
        playerVideoRepo.save(new PlayerVideo(UuidGenerator.next(), playerId, videoId, maxOrderIndex + 1));
    }
    public List<Video> getVideos(String playerId) {
        return videoRepo.findAllByPlayerId(playerId);
    }
    public void removeVideo(String playerId, String videoId) {
        playerVideoRepo.deleteAllByPlayerIdAndVideoId(playerId, videoId);
    }

    private void fillCollectionsWithVideos(java.util.Collection<Collection> collections) {
        for (Collection collection : collections) {
            List<Video> videos = videoRepo.findAllByCollectionId(collection.getId());
            collection.setVideos(videos);
        }
    }
}
