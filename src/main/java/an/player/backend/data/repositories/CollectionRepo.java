package an.player.backend.data.repositories;

import an.player.backend.data.models.Collection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface CollectionRepo extends CrudRepository<Collection, String> {
    @Query(
            value = "SELECT new Collection(c.id, c.name, c.description) " +
                    "FROM Collection c, PlayerCollection pc " +
                    "WHERE c.id = pc.collectionId AND pc.playerId = ?1"
    )
    List<Collection> findAllByPlayerId(String playerId);

    @Query(
            value = "SELECT new Collection(c.id, c.name, c.description) " +
                    "FROM Collection c, PlayerActiveCollection pac " +
                    "WHERE c.id = pac.collectionId AND pac.playerId = ?1"
    )
    Set<Collection> findAllActiveByPlayerId(String playerId);

    /*
    @Query(
            value = "SELECT new DataGroup(g.id, o.minGroupSize, o.maxGroupSize) " +
                    "FROM DataGroup g, Offer o " +
                    "WHERE g.offer.id = o.id AND g.id IN ?1"
    )
    List<DataGroup> findAllOfferMinAndMaxGroupSizeByDataGroupIds(java.util.Collection<String> dataGroupId);
    */
}
