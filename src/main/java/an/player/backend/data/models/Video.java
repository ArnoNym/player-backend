package an.player.backend.data.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Video {
    @Id
    private String id;

    @Column(unique = true, nullable = false)
    private String link;

    @Column(nullable = false)
    private String title;

    private String artist = null;

    private String description = null;
}
