package an.player.backend.web;

import an.player.backend.constants.Constants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint(Constants.SOCKET_END_POINT)
                .setAllowedOrigins("*")
                //.addInterceptors(new SocketConnectChannelInterceptor())
                .withSockJS(); /* Wenn WebSockets nicht funktionieren
        werden die message pfade vorne um /... erweitert*/
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app"); /* Filtert alle Anfragen die damit anfangen und sendet sie
        an @MessageMapping */

        registry.enableSimpleBroker("/topic", "/queue"); /* Definierte Ziele um Messages zu Senden und zu
        empfangen. topic ist oeffentlich und queue privat */
    }


    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        SocketConnectChannelInterceptor scci = createSocketConnectChannelInterceptor();
        if (scci == null) throw new IllegalArgumentException(); //todo loeschen
        registration.interceptors(scci);
    }

    @Bean
    public SocketConnectChannelInterceptor createSocketConnectChannelInterceptor() {
        return new SocketConnectChannelInterceptor();
    }
}
